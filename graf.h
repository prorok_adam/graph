/**
 * @mainpage 
 * Dokumentacja do projektu 3 z przedmiotu PROBE.
 * @author Adam Nowik, grupa J2I3
 */
#ifndef GRAF_H
#define GRAF_H
#include <iostream>
#include <vector>

using namespace std;
/**
 * @page Vector 
 * Klasą bazową służącą do przechowywania elementów
 * dowolnego typu jest klasa vector zawarta w bibliotece STL.
 *
 */

/** 
 * Klasa reprezentująca pojedyńczy wierzchołek, wierzchołek jest zbiorem
 * elementów dowolnego typu, przechowywanych przez kontener vector
 * znajdujący się w bibliotece STL
 */
template <class T>
class Vertex {
	/** 
	 * Element przechowywany przez wierzchołek
	 */
	T elem;
	/** 
	 * Numer identyfikacyjny wierzchołka
	 */
	int index;
	/** Klasa zaprzyjaźniona, zaprzyjaźnienie 
	 * jest potrzebne by móc sprawdzić czy dane
	 * wierzchołki są ze sobą w relacji
	 */
	friend class Edge; 
public:
	Vertex();
	~Vertex();
	/**
	 * Operator sprawdzający czy dany wierzchołek jest w relacji 
	 * z innym wierzchołkiem tego samego grafu. 
	 * @return True jeśli znaleziono relację z 
	 * wierzchołkiem, false jeśli nie istnieje relacja pomiędzy 
	 * wierzchołkami.
	 * @param[in] target - wierzchołek, dla którego będzie
	 * sprawdzana relacja.
	 */
	bool operator>(const Vertex &target) const;
};

/**
 * Klasa reprezentująca krawędzie (relacje) pomiędzy wierzchołkami
 * krawędzie przechowywane są w liście dynamicznej, pojedyńczy element
 * listy zawiera wskaźnik na wierzchołek grafu, do którego prowadzi
 * relacja.
 */
template <class T>
class Edge : public Vertex<T> {
	/**
	 * Vector wskaźników na wierzchołki, jest to zbiór krawędzi
	 * pomiędzy wierzchołkami.
	 */
	vector< Vertex<T>* > relations;
public:
	Edge();
	~Edge();
	/**
	 * Operator dodawania kolejnej relacji.
	 * @return Obiekt typu krawędziowego 
	 * zawierajacy nowe relacje miedzy wierzcholkami
	 * @param[in] source - referencja na obiekt 
	 * reprezentujący wierzchołek
	 */
	Edge<T> operator+(const Vertex<T> &source) const;
	/**
	 * Operator dodawania relacji do danego zbioru relacji.
	 * @return Uaktualniony obiekt reprezentujący krawędzie.
	 * @param[in] source - referencja na obiekt 
	 * reprezentujący wierzchołek
	 */
	Edge<T>& operator+=(const Vertex<T> &source);
	/**
	 * Operator odejmowania relacji.
	 * @return Uaktualniony obiekt reprezentujący krawędzie.
	 * @param[in] source - referencja na obiekt 
	 * reprezentujący wierzchołek

	 */
	Edge<T> operator-(const Vertex<T> &source) const;
	/**
	 * Operator odejmowania relacji z danego zbioru relacji.
	 * @return Uaktualniony obiekt reprezentujący krawędzie.
	 * @param[in] source - referencja na obiekt 
	 * reprezentujący wierzchołek 
	 */
	Edge<T>& operator-=(const Vertex<T> &source);
};

/**
 * Klasa reprezentująca grafy skierowane
 */
template <class T>
class Graph: public Edge<T> {
	/**
	 * ilosc wierzchołków grafu
	 */
	int amount;
public:
	Graph();
	~Graph();
	/**
	 * Operator łączenia grafów.
	 * @return Nowy graf zawierający wierzchołki
	 * i relacje obydwu grafów.
	 * @param[in] source - graf dodawany do danego.
	 */
	Graph<T> operator+(const Graph<T> &source) const;
	/**
	 * Operator łączenia grafu z drugim grafem.
	 * @return Uaktualniony graf zawierający wierzchołki
	 * i relacje obydwu grafów.
	 * @param[in] source - graf dodawany do danego.
	 */
	Graph<T>& operator+=(const Graph<T> &source);
	/**
	 * Operator odejmowania grafów.
	 * @return Nowy graf, będący różnicą 2 grafów.
	 * @param[in] source - graf odejmowany od danego.
	 */
	Graph<T> operator-(const Graph<T> &source) const;
	/**
	 * Operator odejmowania od grafu.
	 * @return Uaktualniony graf, pozbawiony wspólnych wierzchołków
	 * i krawędzi do nich prowadzących.
	 * @param[in] source - graf odejmowany od danego.
	 */
	Graph<T>& operator-=(const Graph<T> &source);
	/**
	 * Operator znajdowania wspólnej części grafów.
	 * @return Graf będący częścią wspólną obydwu grafów.
	 * @param[in] source - graf który będzie porównywany z danym.
	 */
	Graph<T>& operator^(const Graph<T> &source) const;
};

#include "graf.cpp"
#include "edge.cpp"
#include "vertex.cpp"
#endif // GRAF_H
