CC = g++
CFLAGS = -c -g -Wall
EXE = prog_test
SOURCES = main.cpp graf.cpp edge.cpp vertex.cpp
OBJECTS = $(SOURCES:.cpp=.o)
CLEAN	= rm -rf 

$(EXE): $(OBJECTS) 
	$(CC) $(OBJECTS) -o $@

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp 
graf.o: graf.cpp
	$(CC) $(CFLAGS) graf.cpp 
edge.o: edge.cpp
	$(CC) $(CFLAGS) edge.cpp
vertex.o: vertex.cpp
	$(CC) $(CFLAGS) vertex.cpp	

clean:
	$(CLEAN) $(OBJECTS) $(EXE)

